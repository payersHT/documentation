# Overview of JSON_Table section

This section of SQL is used to parse json data into a sql table to be used to join and filter data. More information about the JSON_TABLE can be found 
at the following link to the MYSQL documentation page `https://dev.mysql.com/doc/refman/8.0/en/json-table-functions.html`

```sql
LEFT OUTER JOIN default_shipconfirm.SHC_SHIP_CONFIRM_BATCH_DETAIL SCFD 
    ON SCFD.SHIP_CONFIRM_BATCH_ID = SCF.SHIP_CONFIRM_BATCH_ID
JOIN JSON_TABLE
(SCFD.ENRICHED_PAYLOAD, 
                '$'
                 COLUMNS (
                   ROWID FOR ORDINALITY,
                   NESTED PATH '$.Message.ShipConfirm.ShipConfirmDetails.OriginalOrder[*].Olpn[*]' 
                                        COLUMNS ( MOLPN       VARCHAR(50) PATH '$.OlpnId',
						  						  MSHIPMENT   VARCHAR(50) PATH '$.ShipmentId',
						  						  MLPNQTY     DECIMAL(10,2) PATH '$.TotalLpnQty',
                                                  MORDID      VARCHAR(50) PATH  '$.OlpnDetail[*].OrderId',
                                                  MORDLNID    VARCHAR(50) PATH  '$.OlpnDetail[*].OrderLineId',
                                                  MITEM       VARCHAR(10) PATH  '$.OlpnDetail[*].ItemId',
                                                  MPACKED     DECIMAL(10,2) PATH '$.OlpnDetail[*].PackedQuantity'
												)     
                   ) 
				) AS SCFBDLPN 
```

### Logic Steps for Json_Table

1. Joining the table that contains the JSON column
2. Use the ```JOIN JSON_TABLE ``` function with the following argument

    1. ```SCFD.ENRICHED_PAYLOAD``` => THis is the column name that has the JSON data.
    2. ```'$'``` => used to link the column  name with the ```PATH``` argument.
    3. ```Columns``` => columns that will be used
    4. ```ROWID FOR ORDINALITY``` =>  Specifies an ordinality column. Ordinality columns provide sequential numbering of rows starting at 1
    5. ``` NESTED PATH ``` => Specifies a regular column of the specified scalar type. Regular columns require a JSON Path expression for the column, which is used to search for the column within the current SQL/JSON item produced by the JSON Path expression for the row. 

        a. ```'$.Message.ShipConfirm.ShipConfirmDetails.OriginalOrder[*].Olpn[*]'``` => For any values that are sarouned by [], then you need to include [*] were * is a wild card returning any values.
    6. COLUMNS () => a list of columns that will be included in the table. You will need to provide the following information

        1. name of column
        2. datatype
        3. PATH argument, this is provided by the nested path argument
        4. Where the data can be found inside of the JSON data 



## Example of JSON data structure

This data is found in the default_shipconfirm.SHC_SHIP_CONFIRM_BATCH_DETAIL table in the column ENRICHED_PAYLOAD.

```json 
{
    "Header": null,
    "Message": {
      "ShipConfirm": {
        "ShipConfirmSummary": {
          "OrganizationName": "HOT TOPIC",
          "OriginFacilityId": "999",
          "ShipConfirmHeaderInfo": {
            "CancelQuantity": 0,
            "NumberOfOlpns": 1,
            "NumberOfPallets": 0,
            "NumberOfOrders": 1,
            "TotalWeight": 0,
            "UserId": "job_test",
            "Organization": "HTPC",
            "Facility": "999",
            "BusinessUnitId": null,
            "ShipConfirmBatchId": "SHIPCFR_0000000",
            "ShipConfirmBatchDetailId": "SHIPCFR_0000000_1",
            "ShipConfirmDateTime": "2021-09-24T22:28:01.409"
          }
        },
        "ShipConfirmDetails": {
          "Order": null,
          "OriginalOrder": [
            {
              "PartialShipConfirmStatus": "Full ShipConfirm",
              "Shipment": [],
              "Olpn": [
                {
                  "Actions": {},
                  "PK": null,
                  "CreatedBy": null,
                  "CreatedTimestamp": null,
                  "UpdatedBy": null,
                  "UpdatedTimestamp": null,
                  "Messages": null,
                  "BusinessUnitId": null,
                  "OlpnId": "00004567000000156446",
                  "LpnType": "OLPN",
                  "OriginFacilityId": "999",
                  "DestinationFacilityId": "0816",
                  "Status": "7900",
                  "OrderId": "DO092420210000000007",
                  "ShipmentId": null,
                  "ShipViaId": "U10",
                  "ShippedDateTime": "2021-09-24T22:28:01.409",
                  "ContainerTypeId": "BOX",
                  "ContainerSizeId": "LRG",
                  "LpnLabelTypeId": null,
                  "EstimatedWeight": 21,
                  "EstimatedVolume": 9000000,
                  "PickLocationId": null,
                  "TotalLpnQty": 20,
                  "PalletId": null,
                  "CrossReferenceLpnId": null,
                  "Length": 2100,
                  "Width": 300,
                  "Height": 1300,
                  "IsSingleLine": false,
                  "Weight": 1035,
                  "Volume": 819000000,
                  "StopId": null,
                  "ExternalPackageId": "7",
                  "ExternalShipmentId": "7",
                  "ExternalManifestId": null,
                  "ShippingCharge": 26.51,
                  "AccessorialCharge": null,
                  "OtherCharge": 2.24,
                  "TotalCharge": 26.51,
                  "ChargeCurrency": null,
                  "TrackingNumber": "1Z05F5360300000066",
                  "LoadedDateTime": null,
                  "CarrierId": "CONNECTSHIP_UPS.UPS",
                  "ServiceLevelId": "GND",
                  "BilledWeight": null,
                  "StaticRouteId": null,
                  "PickerId": null,
                  "PackerId": "htpcedgeuser",
                  "PrePackTypeId": null,
                  "ModeId": "PCL",
                  "OlpnDetail": [
                    {
                      "Actions": {},
                      "PK": null,
                      "CreatedBy": null,
                      "CreatedTimestamp": null,
                      "UpdatedBy": null,
                      "UpdatedTimestamp": null,
                      "Messages": null,
                      "BusinessUnitId": null,
                      "OlpnDetailId": "1",
                      "InitialQuantity": 4,
                      "PackedQuantity": 4,
                      "OrderId": "DO092420210000000007",
                      "OrderLineId": "6324993221095046522",
                      "OriginalOrderId": "595665_0816_13",
                      "OriginalOrderLineId": "4",
                      "ItemId": "16502271",
                      "CountryOfOrigin": null,
                      "BatchNumber": null,
                      "ProductStatusId": null,
                      "ItemAttribute1": null,
                      "ItemAttribute2": null,
                      "ItemAttribute3": null,
                      "ItemAttribute4": null,
                      "ItemAttribute5": null,
                      "InventoryTypeId": null,
                      "QuantityUomId": "units",
                      "ExpirationDate": null,
                      "UnitWeight": 1,
                      "UnitVolume": 1,
                      "UnitWeightUomId": "lb",
                      "UnitVolumeUomId": "cuin",
                      "RequiredPackQuantity": 0,
                      "RequiredSubPackQuantity": 0,
                      "RequiredLpnQuantity": 0,
                      "BillOfLadingNumber": null,
                      "OlpnDetailAttribute": [],
                      "Extended": {
                        "ItemStyleSuffix": "16502271",
                        "ItemStyle": "16502266"
                      }
                    }
                  ],
                  "Extended": {}
                }
              ],
              "Actions": {},
              "PK": null,
              "CreatedBy": null,
              "CreatedTimestamp": null,
              "UpdatedBy": null,
              "UpdatedTimestamp": null,
              "Messages": null,
              "BusinessUnitId": null,
              "OriginalOrderId": "595665_0816_13",
              "Priority": null,
              "PickupStartDateTime": null,
              "PickupEndDateTime": null,
              "OrderType": "RetailReceipt",
              "DesignatedShipViaId": null,
              "DeliveryStartDateTime": null,
              "DeliveryEndDateTime": null,
              "OrderPlacedDateTime": null,
              "BillingMethodId": null,
              "BillingAccountNumber": null,
              "OriginFacilityId": "999",
              "OriginAddress": null,
              "DestinationFacilityId": "0816",
              "DestinationAddress": {
                "FirstName": "TBD",
                "LastName": "TBD",
                "Address1": "305 West FM 1382",
                "Address2": "SPC 616",
                "Address3": null,
                "City": "Cedar Hill",
                "State": "TX",
                "PostalCode": "75104",
                "County": null,
                "Country": "US",
                "Phone": "972-291-5586",
                "Email": null
              },
              "DesignatedStopSequenceNumber": null,
              "BillToName": null,
              "BillToTitle": null,
              "BillToFacilityId": null,
              "BillToAddress": null,
              "MustShipTogetherCode": null,
              "DesignatedStaticRouteId": null,
              "DestinationDockId": null,
              "OriginDockId": null,
              "PrePackTypeId": null,
              "MinimumStatus": "Packed",
              "MaximumStatus": "Packed",
              "PreShipConfirmRequired": false,
              "PreShipConfirmed": false,
              "OriginalOrderLine": [
                {
                  "Actions": {},
                  "PK": null,
                  "CreatedBy": null,
                  "CreatedTimestamp": null,
                  "UpdatedBy": null,
                  "UpdatedTimestamp": null,
                  "Messages": null,
                  "OriginalOrderLineId": "4",
                  "ItemId": "16502271",
                  "OrderedQuantity": 4,
                  "CustomerItemNumber": null,
                  "InventoryTypeId": null,
                  "ProductStatusId": null,
                  "BatchNumber": null,
                  "CountryOfOriginId": null,
                  "RequiredShelfDays": null,
                  "UnitWeight": null,
                  "UnitVolume": null,
                  "Cancelled": false,
                  "PickupStartDateTime": null,
                  "PickupEndDateTime": null,
                  "DeliveryEndDateTime": null,
                  "MerchandizingDepartmentId": null,
                  "ReferenceOrderLine": null,
                  "QuantityUomId": "UNIT",
                  "Weight": null,
                  "WeightUomId": null,
                  "VolumeUomId": null,
                  "NmfcFreightClassId": null,
                  "CommodityCodeId": null,
                  "ItemAttribute1": null,
                  "ItemAttribute2": null,
                  "ItemAttribute3": null,
                  "ItemAttribute4": null,
                  "ItemAttribute5": null,
                  "PrePackQuantity": null,
                  "PrePackGroupCode": null,
                  "AssignedShipViaId": null,
                  "UnitPrice": null,
                  "CancelQuantity": null,
                  "OrderLineIds": null,
                  "ShippedQuantity": 4,
                  "Extended": {
                    "PoNbr": null,
                    "ItemStyleSuffix": "16502271",
                    "ItemStyle": "16502266"
                  }
                }
              ],
              "CompleteOrder": true,
              "AssignedShipmentId": null,
              "TotalNumberOfPallets": 0,
              "TotalNumberOfUnits": 4,
              "TotalNumberOfOlpns": 1,
              "Extended": {}
            }
          ]
        }
      }
    }
  }
```