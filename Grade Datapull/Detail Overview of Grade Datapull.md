# Detail Overview of Grade Datapull

This section is used to get the current week and storing it as a varable allows to be referenced multiple time.
``` sql
SET current_week = (SELECT DISTINCT WEEK_NUM 
                    FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV_V2 
                    Where DAY_KEY = cast(getdate() AS date));
```

The Day_REF table is used all day_key values to be used to filter on the Meas_time table later on in the query.
``` SQL
WITH DAY_REF AS  (SELECT DISTINCT day_key
                    from HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV_V2 
                    WHERE ( Day_key <= to_date('1/30/2020','mm/dd/yyyy') AND
                            Day_key >= to_date('1/1/2019','mm/dd/yyyy')) 
                    UNION ALL
                    SELECT DISTINCT DAY_KEY
                    from HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV_V2
                    WHERE WEEK_NUM between ($current_week -13) and $current_week),
```
The time_data temp table contains all of the time filter and join information that is needed when filtering the big data table. The case when is needed for the grade pull to help with later analysis. In the join you will need to filter on MEAS_TIM_RULE_CDE. The value to be filter on can be found in the document understanding time data.
``` SQL
time_data as (SELECT DISTINCT SRC.MEAS_TIM_RULE_CDE
                        , SRC.MEAS_DT
                        , SRC.DAY_KEY
                        , SRC.WK_KEY
                        , SRC.YR_KEY
                        , SRC.WK_SEQ
                        , SRC.MTH_SEQ
                        , SRC.LOC_COMP_STR_REL_DT
                , Case When Day_key between to_date('2/3/2019','mm/dd/yyyy')  
                                    AND  to_date('4/28/2019','mm/dd/yyyy') Then 'Spring'
                    When Day_key between   to_date('5/5/2019','mm/dd/yyyy')  
                                    AND  to_date('6/30/2019','mm/dd/yyyy') Then 'Summer'  
                    When Day_key between   to_date('7/7/2019','mm/dd/yyyy')  
                                    AND  to_date('9/8/2019','mm/dd/yyyy') Then 'BTS'  
                    When Day_key between   to_date('9/15/2019','mm/dd/yyyy')  
                                    AND  to_date('11/10/2019','mm/dd/yyyy') Then 'Fall'  
                    When Day_key between   to_date('11/17/2019','mm/dd/yyyy')  
                                    AND  to_date('1/5/2020','mm/dd/yyyy') Then 'Holiday' 
                    Else 'out of bounds' END AS season_key 
                    FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DM_D_MEAS_TIM_RULE_DAY_MTX SRC
                --Joining for EOP at the day level
                JOIN (SELECT MEAS_TIM_RULE_CDE
                        ,MIN(DAY_KEY) AS MIN_DAY_KEY
                        ,MAX(DAY_KEY) AS MAX_DAY_KEY
                        ,WK_KEY
                        FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DM_D_MEAS_TIM_RULE_DAY_MTX
                    WHERE DAY_KEY IN (SELECT DISTINCT DAY_KEY FROM DAY_REF)                     
                        AND MEAS_TIM_RULE_CDE IN ('10','20')   
                        GROUP BY MEAS_TIM_RULE_CDE,WK_KEY) TGT
                ON SRC.MEAS_TIM_RULE_CDE = TGT.MEAS_TIM_RULE_CDE
                AND SRC.WK_KEY = TGT.WK_KEY
                AND (SRC.MEAS_TIM_RULE_CASE = 'AGG')
                OR  (SRC.DAY_KEY = TGT.MIN_DAY_KEY AND SRC.MEAS_TIM_RULE_CASE = 'BOP')
                OR  (SRC.DAY_KEY = TGT.MAX_DAY_KEY AND SRC.MEAS_TIM_RULE_CASE = 'EOP')),
```
Getting all of the styles information needed for this grade pull. 
```SQL
STYLE_DATA AS (SELECT * 
                FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_PRD_ITM_UDA_LU 
                WHERE DPT_ID IN ('11')
                AND CLS_ID IN ('1165'))
```
The main section of the query contains the data that will be returned. All of the formuals were found by looking at the SQL that is generated inside of Looker.
```SQL
SELECT 
D_TIM_WK_LU.WK_KEY AS WK_KEY,
D_ORG_CHN_LU.CHN_DESC AS CHN_ID_DESC,
D_ORG_LOC_LU.LOC_ID AS LOC_ID,
d_tim_yr_lu.YR_KEY AS YR_KEY,
D_TIM_WK_LU.WK_END_DT AS DATE,
--SALES RETAIL
COALESCE(SUM(CASE WHEN ((MAIN.MEAS_CDE)= 'SLS') THEN
MAIN.F_SLS_RTL ELSE NULL END),0) AS SLS_RTL,
--SALES QTY
COALESCE(SUM(CASE WHEN ((MAIN.MEAS_CDE)= 'SLS')THEN
MAIN.F_SLS_QTY ELSE NULL END),0) AS SLS_QTY,
--GM_AMT
COALESCE(SUM(CASE WHEN (MAIN.MEAS_CDE = 'SLS') THEN
MAIN.F_SLS_RTL ELSE NULL END),0) -
COALESCE(SUM(CASE WHEN (MAIN.MEAS_CDE) = 'SLS'THEN MAIN.F_SLS_CST ELSE NULL END),0 ) AS GM_ANT,
--EOH_QTY
COALESCE(SUM(CASE WHEN (MAIN.MEAS_CDE= 'EOH') THEN
MAIN.F_EOH_QTY ELSE NULL END),0) AS EOH_QTY,
--EOH_CST
COALESCE(SUM(CASE WHEN (MAIN.MEAS_CDE = 'EOH') THEN 
MAIN.F_EOH_CST ELSE NULL END),0) AS EOH_CST,

--STYLE_COUNT
COUNT(DISTINCT CASE WHEN ((MAIN.MEAS_CDE) = 'EOH') THEN
CASE WHEN MAIN.F_MEAS_COL1 > 0 AND MAIN.ORD_BACKORDERED_DT >= (time_data.LOC_COMP_STR_REL_DT)
THEN (PRD_ITM_LU.STY_DESC) END  ELSE NULL END) AS style_count ,

--REGPRO_RTL_SLS_1
ifnull(COALESCE(SUM(CASE WHEN ((MAIN.RTL_TYP_CDE) = 'R') THEN
CASE WHEN (MAIN.MEAS_CDE)='SLS' THEN 
MAIN.F_MEAS_RTL END ELSE NULL END), 0),0) + IFNULL((COALESCE(SUM(CASE WHEN ((MAIN.RTL_TYP_CDE) = 'P') THEN 
CASE WHEN (MAIN.MEAS_CDE)='SLS'
THEN MAIN.F_MEAS_RTL END ELSE NULL END), 0)),0) AS regpro_rtl_sls_1,

--Regpro_gm_1
IFNULL((COALESCE(SUM(CASE WHEN (((MAIN.RTL_TYP_CDE) = 'P')) AND
(((MAIN.MEAS_CDE) = 'SLS')) 
THEN MAIN.F_SLS_RTL - MAIN.F_SLS_CST  ELSE NULL END), 0)),0)
+IFNULL((COALESCE(SUM(CASE WHEN (((MAIN.RTL_TYP_CDE) = 'R')) AND
(((MAIN.MEAS_CDE) = 'SLS')) 
THEN MAIN.F_SLS_RTL- MAIN.F_SLS_CST ELSE NULL END), 0)),0)  AS regpro_gm_1,

--Reg_Eoh_qty
COALESCE(SUM(CASE WHEN (((MAIN.ITMLOC_STTS_CDE) = 'R')) AND (((MAIN.MEAS_CDE) = 'EOH')) THEN MAIN.F_EOH_QTY ELSE NULL END), 0) AS reg_eoh_qty,

--Reg_eoh_cst
COALESCE(SUM(CASE WHEN (((MAIN.ITMLOC_STTS_CDE) = 'R')) AND (((MAIN.MEAS_CDE) = 'EOH')) THEN MAIN.F_EOH_CST  ELSE NULL END), 0) AS reg_eoh_cst,

--Style_count_reg_price
COUNT(DISTINCT CASE WHEN (((MAIN.MEAS_CDE) = 'EOH')) AND
(((MAIN.ITMLOC_STTS_CDE) = 'R')) THEN CASE WHEN MAIN.F_MEAS_COL1 > 0 AND
MAIN.ORD_BACKORDERED_DT >= (time_data.LOC_COMP_STR_REL_DT)
THEN (PRD_ITM_LU.STY_DESC) END  ELSE NULL END) AS style_count_reg_price,

--Weeks_fwd
COALESCE(SUM(CASE WHEN (((MAIN.MEAS_CDE) = 'GL_SLS')) AND
(NOT (PRD_ITM_LU.ITM_KEY  IS NULL)) 
AND (NOT (d_org_loc_lu.LOC_KEY  IS NULL)) THEN MAIN.F_MEAS_RTL  ELSE NULL END), 0) AS gl_sls_rtl,

(WK_REG.WEEK_NUM - $current_week) as Weeks_back,
((WK_REG.WEEK_NUM - $current_week) + 52) as Weeks_fwd,
time_data.season_key,

CASE WHEN Weeks_fwd between -64 and -118 then 'Last Year' 
    ELSE 'Out of Scope' end as Year_context

FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DM_F_MEAS_CCY_IL_B AS MAIN
```
Time_data join is how the big table nows what values to aggregate on. You will need to join on two values, the MEAS_DT value and MEAS_TIM_RULE_CDE
```SQL
INNER JOIN time_data
    ON time_data.MEAS_DT = MAIN.MEAS_DT
    AND time_data.MEAS_TIM_RULE_CDE = MAIN.MEAS_TIM_RULE_CDE
```
This join give use all information about a give day.
``` SQL
INNER JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV AS d_tim_day_lu
    ON TIME_DATA.DAY_KEY = d_tim_day_lu.DAY_KEY
```
This join give use all information about a given week.
``` SQL
INNER JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_D_TIM_WK_LU_BRV AS D_TIM_WK_LU
    ON TIME_DATA.WK_KEY = D_TIM_WK_LU.WK_KEY
```
This join give use all information about a given year.
``` SQL
INNER JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_D_TIM_YR_LU_BRV AS d_tim_yr_lu
    ON TIME_DATA.YR_KEY = d_tim_yr_lu.YR_KEY
```
This join give use all information about store locations.
``` SQL
INNER JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_ORG_LOC_LU AS d_org_loc_lu
    ON MAIN.LOC_KEY  = D_ORG_LOC_LU.LOC_KEY
```
This join give use all information about a given style.
``` SQL
INNER JOIN STYLE_DATA AS PRD_ITM_LU
    ON MAIN.ITM_KEY = PRD_ITM_LU.ITM_KEY
```
This join is needed because it contains week number.This is only needed for the grade data analysis.
``` SQL
INNER JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV_V2 WK_REG
    on WK_REG.WK_KEY = D_TIM_WK_LU.WK_KEY
```
``` SQL
LEFT JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_ORG_CHN_LU AS D_ORG_CHN_LU
    ON MAIN.CHN_KEY = D_ORG_CHN_LU.CHN_KEY
```
To filter the data properly you need to filter on 3 main points (for this query at least). First is the Day_key, so the query can pull the proper data needed.Second is MEAS_CDE this is lokoing at what measures you are pulling in for the final query. lastly, is FACT_CDE this is the numerical value for the MEAS_CDE and a full list of them can be found in the documentation Understanding Measure data.
``` SQL
WHERE d_tim_day_lu.DAY_KEY IN (SELECT DISTINCT DAY_KEY FROM time_data )
AND D_ORG_CHN_LU.CHN_DESC = 'Hot Topic'
AND MAIN.MEAS_CDE IN (NULL,'EOH','GL_SLS','SLS','EOH')
AND MAIN.FACT_CDE IN (NULL,210,-135,110,210)
group by
D_TIM_WK_LU.WK_KEY,
time_data.season_key,
D_TIM_WK_LU.WK_END_DT,
D_ORG_CHN_LU.CHN_DESC,
D_ORG_LOC_LU.LOC_ID,
d_tim_yr_lu.YR_KEY,
Weeks_back,
Weeks_fwd,
Year_context
ORDER BY TO_NUMBER(LOC_ID), TO_NUMBER(WK_KEY)```