# Grade Process #

## Overview ##
This process will go over the whole grade process, starting with what is needed to make a new grade and what tables need to be updated. The workflow used for getting data from Looker to a CSV file is ready to be used in the grade building process. Finally, look at the grade load process for uploading and storing grade data. The


### process for making and adding a new grade ###
When a person wants to make a new grade, they will be to give the following information:
* What is the name they want to grade CSV to be called
* A link to the looker data pull.

Three items need to be updated when the information is given from the analysis.
1) Adding the new grade to the automated data pulling process.
2) Adding the new grade to the excel datasheet
3) Adding the new grade to the grade header table in Snowflake.

### Setting up the automated data pulling process ###
Once that information is given, you will need to set up the Looker data to be pulled automatically every Sunday. The Data pull will be saved in the Shared folder under allocation in the grade data pulls folder. Select the three vertical dots on the right-hand side and select "edit schedule" when a Looked is saved in this location. You will fill in the schedule name; under the "format data as" section, you will select "Data Table." Next, under "Deliver this schedule," select the weekly button and the time that is available for the data for the data pull. in the Filters section, make sure all filters are set up correctly. Lastly, In the "Advanced Options," ensure "All Results" in the limit section is selected.


### Adding New Grade to Excel Sheet ###
You will need to add a new grade to the Looker Grade List. This file is located in the following directory "\\ntserver\pa\General User\SP&A\Allocation Dev\Projects\grade_Upload\Looker Grade List." There are three types of grades, Acc, App, and HT Global; depending on what grade is being added is what determines its location. Please ask the analysis which is requesting the new grade for what type of grade it is. Columns A-C is the different types of names the grade will have. Column A will be the grade's general name, and column C will be the name of the grade CSV that the analysis will use when updating grades. Column B is the name of the CSV that Looker will give the data pull. This name must start with "GRD_CSV_" and every white space" " needs to be replaced with "_." This is for text matching that happens in the grade renaming process. Columns D-M is for all of the information related to the pulled data. If a column has multiple values, you will need to ensure they are pipe separated "|". For example, if a grade pulls data from classes 2180 and 2179, you will fill in column F (Classes) like "2180|2179". Columns N - Q is just general information:
* Column N (Look Created) => has the Looker data pull been created (Yes or No)
* column O (Scheduled) => has the data pulled been scheduled (Yes or No)
* Column P (Grade Created) => has the grade file been created ( Yes or No)
* Column Q (Linked) => This column is not in use.
The last column that will be adjusted is column R (Looker Schedule time). Make sure to include if the time is set to AM or PM. We can only run one (1) grade pull every 10 minutes at this time. If we increase the number, this will cause Looker to have problems not just for the allocation team but the whole company. 

### Adding New Grade to Snowflake database ###
This step is best done when the excel sheet is filled out and the data pull has been set up. Here is an example of inserting a new grade into GRADE_HEAD_DATA table. The grade ID column will need to be a unique value. The last three columns, JDA_DEPARTMENT, JDA_CLASS, and JDA_SUBCLASS, contain the values for loading grades into JDA. 

```SQL
INSERT INTO HT_SANDBOX_DB.ALLOC_MODEL.GRADE_HEAD_DATA         ('100010002','GRD_CSV_Acc_-_D01_Fashion_Jewelry','Acc - D01 Fashion Jewelry','Hot Topic','1','174|175|176|177|178|179|180','116|113|110|107|104|101','','','','','','','1','1','1','Sunday 06:00 PM','','','')  
```


### Grade Data CSV Data Pipeline ###
This section goes over the data pipeline for how the data goes from Looker to the CSV that the allocation uses to update their grades. 

#### Step One ####

A Look is set up to pull automatically every Sunday and will be delivered into the Store Planning email. 

#### Step Two ####

On Monday morning, a process will run that will be moving all of the grade data pulls in the store planning email into the following folder `\ntserver\pa\General Use\Store Planning\Grades\HT Store Plans\CSV Grade Pulls`
The data will not have the general name for the grade CSV but the name generated from Looker. 

#### Step Three ####

Every Monday at 9 AM, the grade name cleaner process is run, changing the grade name from the name generated from Looker to the general grade name. After this process is completed, the team is free to update their grades.

Below is the python code for the Grade name cleaners.

```python
1   import numpy as np
2   import pandas as pd
3   import os
4
5   def clean_grade_names():
6        os.chdir("//ntserver/pa/General Use/Store Planning/Grades/HT Store 6Plans/CSV Grade Pulls")
7       file_list = os.listdir()
8       print(file_list)
9       grade_mete_data = pd.read_excel("//ntserver/pa/General Use/SP&A/Allocation Dev/Projects/Grade_upload/Looker Grade List/Looker Grade List.xlsx",
10       usecols="A:C",engine='openpyxl')
11
12    print(grade_mete_data)
13    for i in range(len(grade_mete_data)):
14        for j in file_list:
15            # print(i)
16            if j.endswith(".csv") and j.startswith("GRD_"):
17                if (grade_mete_data.loc[i,"Grade Data Pull Name"]== j[0 : j.rindex("_")]):
18                    new_name = grade_mete_data.loc[i,"Clean Name"] + ".csv"
19                    print(i)
20                    old_name = j
21                    print(new_name)
22                    try:
23                        os.remove(new_name)
24                        os.rename(old_name, new_name)
25                    except:
26                        os.rename(old_name, new_name)
``` 

* Line 1 to 3 are loading in standard python packages.
  
* Line 6 to 7 are getting all of the CSV files in the CSV Grade pull folder.
  
* Line 9 is reading in the excel file that was filled out in the creation of the grade. 
  
* Line 12 to 26 is the process that will rename the CSV files. 
  
* Line 16 will make sure that the updated file is a CSV from Looker. This is done by making sure the files start with "GRD_."
  
* Line 24 to 26 will remove the old CSV files and keep the new file. 



## Grade Upload Process ##

This section will cover how grades are uploaded from the grade sheet into the snowflake database. If you would like to know more about the grade upload process, please reach out to whoever maintains that sheet. 

When the grade sheet is updated, and the "submit Grade" is clicked, a macro will ask the user for a snowflake user name and password. The macro will take all of the information in the grade model sheet and the DM sheet and upload it into the following tables HT_SANDBOX_PRD_DB.ALLOC_MODEL_STAGE.STAGE_GRADE_LOAD, ALLOC_MODEL_STAGE.STAGE_GRADE_META,ALLOC_MODEL_STAGE.STAGE_DM_DATA.

On Wednesday evening, a function is called that will take the grade data from the staging tables and load it into the production table.