# **JDA Variable** ##

```SQL
1 WHENTEST(HT~LOCATION_ID NOTIN('1997') AND HT~LOCATION_ID <'875',
2    HT_MODEL_Auto,
3     WHENTEST(HT~LOCATION_ID IN ('1997'),
4        IF(MAX((ENV(AVAILABLE) - Total_Store_Need)*TARGET,0)<5)THEN 0 
5            ELSE MAX((ENV(AVAILABLE) - Total_Store_Need)*TARGET,0)
6             ,0))
```

# **Breakdown Line by Line**

## **Line one** ##
---
WHENTEST function is a conditional statement with a general form as ``` WHENTEST(logical statement,expresion1,expresion2)``` It will return the value of expression1 if the logical statement evaluates to TRUE, and the value of expression2 if the logical statement evaluates to FALSE. 

We want to make sure that the locations in the allocation line are not store number 1997 AND are not a Canada location (done by saying the location ID must be below 875). 875 was chosen as the number because of avoiding transfer locations.

## **Line Two** ##
---
If a store location does not have an ID of 1997 and is less than 875, we will run the standard HT_MODEL_Auto variable.

## **Line Three** ##
---

For any locations that do not meet the conditions from line 2, we then move into line 3. Using the WHENTEST function, check if the location ID is equal to 1997. 

## **Line Four** ##
---
For stores with location id equal to 1997, we will be testing if the number of units being allocated is greater than 5. Here is the breakdown of the if statement.

```SQL 
        IF(MAX((ENV(AVAILABLE) - Total_Store_Need)*TARGET,0)<5)THEN 0 
            ELSE MAX((ENV(AVAILABLE) - Total_Store_Need)*TARGET,0)
```

ENV(AVAILABLE) => This is the number of available units for a given Worklist line. 

Total_Store_Need => This is a separate variable that finds the total store need got a given worklist line. 

The IF statement will take the difference between the available units and the total store need and then multiply it by the TARGET. Wrapping this in a MAX function with a zero to ensure that units being allocated will be at least zero (0) and not negative. The last check will be checking if units are greater than 5. 


## **Line Five** ##
---
If a store does not have a location ID of 1997, we will run the same calculation but without the limit min of 5 units. 

## **Line Six** ##
---

This line will be closing out both of the WHENTEST statements, and all other values will get zero (0) units.

## **Total_Store_Need** ##

This is the calculation for the total store need for US stores. It works by taking the total units allocated to Canada and IFC and then subtracting those units from the total available units of the PO.

```SQL
WHENTEST(HT~LOCATION_ID IN('1997','4400','4499','2500','2501','2502','2503',
                            '2504','2505','2506','2507','2508','2509','2510',
                            '2511','2512','2513','2514','2515','2516','2517',
                            '2518','2519','2520','2521','2522','2523','2524',
                            '2525','2526','2527','2528','2529','2530'),
 GROUPSUM(HT_MODEL_Auto,SIZE_NBR),0)
```


