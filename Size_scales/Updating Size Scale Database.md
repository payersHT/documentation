# Updating Size Scale Database

This function will run all of the commands needed to update the production level database.All of the commands can be found in the size scale loader document. 

```python
def updating_size_scale_database(SF_UN, SF_PW):
	ctx = snowflake.connector.connect(
		user=SF_UN,
		password=SF_PW,
		# this can be found in the URL of snowflake website
		account="hottopic.east-us-2.azure",
		database=DATABASE,
		schema="ALLOC_STG_MODEL",
		# This needs to be set so you can write to the staging and normal tables
		role="HT_DBA_PRD",
	)
	cs = ctx.cursor()

	try:
		print('setting warehouse')
		cs.execute(f" USE WAREHOUSE {DB_WAREHOUSE}")
		print(DataFrame(cs.fetchall()))

		print("updating size scale")
		cs.execute(update_size_scale_sql_1)
		print(DataFrame(cs.fetchall()))

		print('insert new size scale')
		cs.execute(INSERT_NEW_SIZE_SCALE_DETAIL_2)
		print(DataFrame(cs.fetchall()))

		print('insert new size scale')
		cs.execute(INSERT_NEW_SIZE_SCALE_PROF_3)
		print(DataFrame(cs.fetchall()))

		print('Setting first max value')
		cs.execute(SETTING_MAX_ACTIVE_4)
		print(DataFrame(cs.fetchall()))

		print('Creating new temp table')
		cs.execute(CREATING_NEW_SEASON_TEMP_5)
		print(DataFrame(cs.fetchall()))

		print('Inserting into temp table')
		cs.execute(INSERT_NEW_SEASON_TEMP_6)
		print(DataFrame(cs.fetchall()))

		print('Insert new season into header')
		cs.execute(INSERT_NEW_SEASON_HEADER_7)
		print(DataFrame(cs.fetchall()))

		print('Insert new season into profile')
		cs.execute(INSERT_NEW_SEASON_PROFILE_8)
		print(DataFrame(cs.fetchall()))

		print('Insert new season into detail')
		cs.execute(INSERT_NEW_SEASON_DETAIL_9)
		print(DataFrame(cs.fetchall()))

		print('setting max active')
		cs.execute(SETTING_MAX_ACTIVE_10)
		print(DataFrame(cs.fetchall()))

		print('making new temp table')
		cs.execute(CREATING_TEMP_TABLE_11)
		print(DataFrame(cs.fetchall()))

		print('insert into temp table')
		cs.execute(INSERT_INTO_TEMP_TABLE_12)
		print(DataFrame(cs.fetchall()))

		print('updateing detail')
		cs.execute(UPDATEING_MODEL_DETAIL_13)
		print(DataFrame(cs.fetchall()))

		print('updateing prof table')
		cs.execute(UPDATEING_MODEL_PROF_14)
		print(DataFrame(cs.fetchall()))

		print('updating header table')
		cs.execute(UPDATING_MODEL_HEADER_15)
		print(DataFrame(cs.fetchall()))

		print('remove detail records')
		cs.execute(REMOVE_DETAIL_RECORDS_16)
		print(DataFrame(cs.fetchall()))

		print('insert detail same week')
		cs.execute(INSERT_DETAIL_SAME_WEEK_17)
		print(DataFrame(cs.fetchall()))

		print('remove profile same week')
		cs.execute(REMOVE_PROFILE_SAME_WEEK_18)
		print(DataFrame(cs.fetchall()))

		print('insert profile same week')
		cs.execute(INSERT_PROFILE_SAME_WEEK_19)
		print(DataFrame(cs.fetchall()))

		print('set active to zero')
		cs.execute(ACTIVE_SET_0_20)
		print(DataFrame(cs.fetchall()))

		print('set active to 1')
		cs.execute(ACTIVE_SET_1_21)
		print(DataFrame(cs.fetchall()))

		print('drop table 1')
		cs.execute(DROP_TABLE_22)
		print(DataFrame(cs.fetchall()))

		print('drop table 2')
		cs.execute(DROP_TABLE_2_23)
		print(DataFrame(cs.fetchall()))

	finally:
		cs.close()
	ctx.close()
```