# loading_size_data

 This is the function that is used to load all of the size scale into snowflake.

```python
def loading_size_data(load_DF, type_load, SF_UN, SF_PW):
    #Will load diata for size scales to different types of database tables

    #Parameters:
    #    load_DF (dataframe): A dataframe that will be loaded into a database. Must be given
    #    type_load (str): A string that will be used to know what table to load the size scale 
    #    SF_UN (str): The user name for snowflake. Must be given type
    #    SF_PW (str): The Password for Snowflake

    #Returns:
    #    reverse(str1):The string which gets reversed. 
	if type_load == "" or type_load not in ["size_scale_detail","size_scale_prof","size_scale_header"]:
		raise ValueError("Please select that data that is being loaded")
		return -1
	ctx = snowflake.connector.connect(
		user=SF_UN,
		password=SF_PW,
		# this can be found in the URL of snowflake website
		account="hottopic.east-us-2.azure",
		database=DATABASE,
		schema="ALLOC_STG_MODEL",
		# This needs to be set so you can write to the staging and normal tables
		role="HT_DBA_PRD",
	)
	cs = ctx.cursor()

	if type_load == "size_scale_detail":
		try:
			# write csv file
			load_DF.to_csv("temp_detail_load.csv", index=False, header=False)
			# Changing to the currect WAREHOUSE
			cs.execute(f" USE WAREHOUSE {DB_WAREHOUSE}")
			print(DataFrame(cs.fetchall()))
			cs.execute("TRUNCATE HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.SIZE_SCALE_DETAIL;")
			# have to state the location of the file path, the "@%" is letting snowflake know to
			# put it into the staging table for tables it will go to.
			cs.execute("PUT file://temp_detail_load.csv @%SIZE_SCALE_DETAIL OVERWRITE = TRUE")
			# Copy the data into a new table form the stage table.
			# cs.execute("TRUNCATE SIZE_SCALE_HEADER_TEMP;")
			cs.execute("COPY INTO SIZE_SCALE_DETAIL FROM @%SIZE_SCALE_DETAIL;")
			os.remove("temp_detail_load.csv")
		finally:
			cs.close()
		ctx.close()
	elif type_load == "size_scale_prof":
		try:
			# write the csv file
			load_DF.to_csv("temp_prof_load.csv", index=False, header=False)
			# Changing to the currect WAREHOUSE
			cs.execute(f" USE WAREHOUSE {DB_WAREHOUSE}")
			print(DataFrame(cs.fetchall()))
			cs.execute("TRUNCATE HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.SIZE_SCALE_PROF;")
			# have to state the location of the file path, the "@%" is letting snowflake know to
			# put it into the staging table for tables it will go to.
			cs.execute("PUT file://temp_prof_load.csv @%SIZE_SCALE_PROF OVERWRITE = TRUE")
			# Copy the data into a new table form the stage table.
			# cs.execute("TRUNCATE SIZE_SCALE_HEADER_TEMP;")
			cs.execute("COPY INTO SIZE_SCALE_PROF FROM @%SIZE_SCALE_PROF ;")
			os.remove("temp_prof_load.csv")
		finally:
			cs.close()
		ctx.close()
	elif type_load == "size_scale_header":
		try:
			# write the csv file
			load_DF[["temp_id","scale_id","size_profile_detail_id",
					"created_year","created_season","scale_name","scale_rename","department",
					"class","size_count","size_range","profile_count","gender","Creator","body_style","genre","license","Notes","row_num","created_dttm","New_size_scale"]].to_csv('temp_header_load.txt',index = False,header=False,na_rep = '',sep="|")
			# Changing to the currect WAREHOUSE
			cs.execute(f"USE WAREHOUSE {DB_WAREHOUSE};")
			print(DataFrame(cs.fetchall()))
			cs.execute("TRUNCATE HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.STG_D_SIZE_SCALE_HEAD;")
			# have to state the location of the file path, the "@%" is letting snowflake know to
			# put it into the staging table for tables it will go to.
			cs.execute("PUT file://temp_header_load.txt @%STG_D_SIZE_SCALE_HEAD OVERWRITE = TRUE")
			# Copy the data into a new table form the stage table.
			# cs.execute("TRUNCATE SIZE_SCALE_HEADER_TEMP;")
			cs.execute("COPY INTO STG_D_SIZE_SCALE_HEAD FROM @%STG_D_SIZE_SCALE_HEAD file_format = (format_name = csvformat);")
			os.remove("temp_header_load.txt")
		finally:
			cs.close()
		ctx.close()
```