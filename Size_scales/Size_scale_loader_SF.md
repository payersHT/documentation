# Size Scale loading SQL statements

This documents contains all of the sql statements that are used to update the allocation teams size scales. This is broken up into multiple statements becuase of limitations of the Snowflake API. 


1. [Adding New Size Scales to Head Table](#adding_NSS_HT)
2. [Adding New Size Scales to detail Table](#adding_NSS_DT)
3. [Adding New Size Scales to Profile Table](#adding_NSS_PT)
4. [Setting Max for New Season](#setting_max_NSS)
5. [Creating New Season Temp](#creating_NSS_temp)
6. [Inserting Data Into Temp](#inserting_into_temp)
7. [Inserting New Season Header](#adding_NSSS_HT)
8. [Inserting New Season Profile](#adding_NSSS_PT)
9. [Inserting New Season Detail](#adding_NSSS_DT)
10. [Setting Active Size Scale](#setting_Active_SS)
11. [Creating Temp Table](#creating_TT)
12. [Inserting Into Temp Table](#inserting_TT)
13. [Updating Model Detail Table](#updating_DT)
14. [Updating Size Scale Profile Table](#updating_PT)
15. [Updating Size Scale Header Table](#updating_HT)
16. [Removeing Detail Size Scale Records](#removeing_SSRD)
17. [Inserting Size Scale Detail Data](#inserting_SSDD)
18. [Removing Profile Size Scale Data](#removing_SSDP)
19. [Inserting Size Scale into Profile Table](#inserting_SSPD)
20. [Setting Active Value](#setting_active)
21. [Dropping Temp Table](#dropping_table)


## Adding New Size Scales to Head Table <a name='adding_NSS_HT'></a>

This query will add new size scale to the head table.
```sql
 INSERT INTO HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_HEADER (SIZE_SCALE_ID,
																ACTIVE_SIZE_SCALE,
																SEASON_YEAR,
																SEASON,
																SCALE_NAME,
																DEPARTMENT,
																CLASS,
																SIZE_COUNT,
																SIZE_RANGE,
																SIZE_LIST,
																PROFILE_COUNT,
																GENDER,
																CREATOR,
																BODY_STYLE,
																GENRE,
																LICENSE,
																NOTES,
																UPDATED_DATE)
	SELECT SCALE_ID,
		SIZE_PROFILE_DETAIL_ID,
		CREATED_YEAR,
		CREATED_SEASON,
		SCALE_NAME,
		DEPARTMENT,
		CLASS,
		SIZE_COUNT,
		SIZE_RANGE,
		SIZE_LIST,
		0,
		GENDER,
		CREATOR,
		BODY_STYLE,
		GENRE,
		LICENSE,
		NOTES,
		CREATED_DTTM
	FROM HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.STG_D_SIZE_SCALE_HEAD
	WHERE NEW_SIZE_SCALE = '1';
```
## Adding New Size Scales to detail Table <a name="adding_NSS_DT"></a>

Adding new size scale to detail table.

```sql
INSERT INTO HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_DETAIL (SCALE_ID,
																SIZE_PROFILE_DETAIL_ID,
																ACTIVE,
																PROFILE_NAME,
																SIZE_INDEX,
																SIZE_ID,
																SIZE_PERCENT,
																MIN_UNITS,
																SEASON,
																SEASON_YEAR)
	SELECT SCALE_ID,
		SIZE_PROFILE_DETAIL_ID,
		ACTIVE,
		PROFILE_NAME,
		SIZE_INDEX,
		SIZE_ID,
		SIZE_PERCENT,
		MIN_UNITS,
		CREATED_SEASON,
		CREATED_YEAR
	FROM (SELECT TB1.SCALE_ID,
		TB1.SIZE_PROFILE_DETAIL_ID,
		0 AS ACTIVE,
		TB1.PROFILE_NAME,
		NULL AS SIZE_PROFILE_NUM,
		TB1.SIZE_INDEX,
		TB1.SIZE_ID,
		TB1.SIZE_PERCENT,
		TB1.MIN_UNITS,
		TB2.CREATED_SEASON,
		TB2.CREATED_YEAR 
		FROM HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.SIZE_SCALE_DETAIL AS TB1
		INNER JOIN (SELECT * 
				FROM HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.STG_D_SIZE_SCALE_HEAD
				WHERE NEW_SIZE_SCALE = '1') AS TB2
		ON TB1.TEMP_ID = TB2.TEMP_ID);
```
## Adding New Size Scales to Profile Table <a name="adding_NSS_PT"></a>

Adding new size scales to profile table.

```sql
INSERT INTO HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_PROF (SIZE_SCALE_ID,
															SIZE_PROFILE_DETAIL_ID,
															SCALE_NAME,
															STORE_NUMBER,
															PROFILE_NAME)
	SELECT SCALE_ID,
	SIZE_PROFILE_DETAIL_ID,
	SCALE_NAME,
	STORE_NUMBER,
	PROFILE_NAME
	FROM (SELECT  TB1.SCALE_ID,
	TB1.SIZE_PROFILE_DETAIL_ID,
	TB1.SCALE_NAME,
	TB1.STORE_NUMBER,
	TB1.PROFILE_NAME
		FROM HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.SIZE_SCALE_PROF AS TB1
		INNER JOIN (SELECT *
				FROM HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.STG_D_SIZE_SCALE_HEAD
				WHERE NEW_SIZE_SCALE = '1') AS TB2
			ON TB1.TEMP_ID = TB2.TEMP_ID);
```
## Setting Max for New Season <a name="setting_max_NSS"></a>
This is getting the max value for size scales that have a new season attached to them.
```sql
SET MAX_ACITVE_SIZE_SCALE = (SELECT MAX(ACTIVE_SIZE_SCALE) 
											FROM HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_HEADER);
```

## Creating New Season Temp Table <a name="creating_NSS_temp"></a>
This table will hold all size scales that need to be loaded that are a new season for a given year.
``` sql
CREATE TEMPORARY TABLE HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_NEW_YEAR_TEMP(
    TEMP_ID VARCHAR(100),
	MAX_ACTIVE_SS  NUMBER(20,0),
	SIZE_SCALE_ID NUMBER(38,0),
	SIZE_SCALE_NAME VARCHAR(100),
	CREATED_YEAR NUMBER(38,0),
	CREATED_SEASON VARCHAR(50),
	DEPARTMENT VARCHAR(50));
```

## Inserting Data into Temp Table <a name="inserting_into_temp"></a>
Inserting head data into the temp table. Also, this is generating the new active size scale numbers.
``` sql
INSERT INTO HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_NEW_YEAR_TEMP(
    TEMP_ID,
	MAX_ACTIVE_SS,
	SIZE_SCALE_ID ,
	SIZE_SCALE_NAME,
	CREATED_YEAR ,
	CREATED_SEASON ,
	DEPARTMENT)

	SELECT DISTINCT TEMP_ID,
	$MAX_ACITVE_SIZE_SCALE + ROW_NUMBER() OVER ( ORDER BY (SELECT NULL)) AS NEW_ACTIVE_VALUE,
	NEW.SCALE_ID,
	NEW.SCALE_NAME,
	NEW.CREATED_YEAR,
	NEW.CREATED_SEASON,
	NEW.DEPARTMENT
	FROM (SELECT DISTINCT WK_KEY,
							TEMP_ID,
							SCALE_ID,
							CREATED_YEAR,
							CREATED_SEASON,
							SCALE_NAME,
							DEPARTMENT,
							CREATED_DTTM
							FROM HT_SANDBOX_PRD_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV AS TB1
								JOIN HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.STG_D_SIZE_SCALE_HEAD AS TB2
									ON CAST(TB2.CREATED_DTTM AS DATE) = TB1.DAY_KEY
							WHERE NEW_SIZE_SCALE = '0') new 
					where NOT EXISTS (SELECT * 
					from HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_HEADER cur
					WHERE CUR.SIZE_SCALE_ID = NEW.SCALE_ID
					AND CUR.SEASON_YEAR  = NEW.CREATED_YEAR					
					AND CUR.SEASON = NEW.CREATED_SEASON					
					AND CUR.DEPARTMENT = NEW.DEPARTMENT);
```

## Inserting New Season Header <a name="adding_NSSS_HT"></a>
Inserting size scales that are a new season.
```sql
INSERT INTO HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_HEADER(SIZE_SCALE_ID, 
                                                            ACTIVE_SIZE_SCALE,
                                                            SEASON_YEAR,
                                                            SEASON,
                                                            SCALE_NAME,
                                                            DEPARTMENT,
                                                            CLASS,
                                                            SIZE_COUNT,
                                                            SIZE_RANGE,
                                                            SIZE_LIST,
                                                            PROFILE_COUNT,
                                                            GENDER,
                                                            CREATOR,
                                                            BODY_STYLE,
                                                            GENRE,
                                                            LICENSE,
                                                            NOTES,
                                                            UPDATED_DATE)

	SELECT  STG.SCALE_ID,
		TEMP.MAX_ACTIVE_SS,
		STG.CREATED_YEAR,
		STG.CREATED_SEASON,
		STG.SCALE_NAME,
		STG.DEPARTMENT,
		STG.CLASS,
		STG.SIZE_COUNT,
		STG.SIZE_RANGE,
		STG.SIZE_LIST,
		0,
		STG.GENDER,
		STG.CREATOR,
		STG.BODY_STYLE,
		STG.GENRE,
		STG.LICENSE,
		STG.NOTES,
		STG.CREATED_DTTM
	FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_NEW_YEAR_TEMP AS TEMP
	INNER JOIN HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.STG_D_SIZE_SCALE_HEAD STG
    ON STG.TEMP_ID = TEMP.TEMP_ID;
```

# Inserting New Season Profile <a name="adding_NSSS_PT"></a>
Inserting size scales that are a new season into the profile table.
```sql
INSERT INTO HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_PROF(SIZE_SCALE_ID,
                                                        SIZE_PROFILE_DETAIL_ID,
                                                        SCALE_NAME,
                                                        STORE_NUMBER,
                                                        PROFILE_NAME)
	SELECT STG.SCALE_ID,
	TEMP.MAX_ACTIVE_SS,
	STG.SCALE_NAME,
	STG.STORE_NUMBER,
	STG.PROFILE_NAME
	FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_NEW_YEAR_TEMP AS TEMP
	INNER JOIN HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.SIZE_SCALE_PROF AS STG
		ON STG.TEMP_ID = TEMP.TEMP_ID;
```

## Inserting New Season Detail Table <a name="adding_NSSS_DT"></a>
Inserting size scales that are a new season into the detail table.

```sql
INSERT INTO HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_DETAIL (SCALE_ID,
																SIZE_PROFILE_DETAIL_ID,
																ACTIVE,
																PROFILE_NAME,
																SIZE_INDEX,
																SIZE_ID,
																SIZE_PERCENT,
																MIN_UNITS,
																SEASON,
																SEASON_YEAR)
	SELECT STG.SCALE_ID,
	TEMP.MAX_ACTIVE_SS,
	0 AS ACTIVE,
	STG.PROFILE_NAME,
	STG.SIZE_INDEX,
	STG.SIZE_ID,
	STG.SIZE_PERCENT,
	STG.MIN_UNITS,
	TEMP.CREATED_SEASON,
	TEMP.CREATED_YEAR
	FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_NEW_YEAR_TEMP AS TEMP
	INNER JOIN HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.SIZE_SCALE_DETAIL AS STG
		ON TEMP.TEMP_ID = STG.TEMP_ID;
```

## Setting Active Size Scale <a name="setting_Active_SS"></a>

Finding and setting max active size scale value. This will be used when we update size scale data. 

```sql
SET MAX_ACITVE_SIZE_SCALE = (SELECT MAX(ACTIVE_SIZE_SCALE) 
											FROM HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_HEADER);
```

## Creating Temp Table <a name="creating_TT"></a>

Creating a temp table that will be used for updating size scales. We need to use a temp table because of limitation of the snowflake API. This table will be dropped at the end of the process.

```sql
CREATE TEMPORARY TABLE HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_TEMP(
	NEW_WK_KEY  NUMBER(20,0),
	CUR_WK_KEY  NUMBER(20,0),
	MAX_ACTIVE_SS  NUMBER(20,0),
	TEMP_ID  VARCHAR(256),
	SCALE_ID  BIGINT,
	active_size_scale BIGINT,
	SCALE_NAME  VARCHAR(256),
	CREATED_YEAR  NUMBER(38,0),
	CREATED_SEASON  VARCHAR(256),
	DEPARTMENT  VARCHAR(100),
	DATA_FLAG  INT);
```
## Inserting Into Temp Table <a name="inserting_TT"></a>



```sql
INSERT INTO HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_TEMP (NEW_WK_KEY,
								CUR_WK_KEY,
								TEMP_ID,
								SCALE_ID,
								active_size_scale,
								SCALE_NAME,
								CREATED_YEAR,
								CREATED_SEASON,
								DEPARTMENT,
								DATA_FLAG)
								
	SELECT NEW.WK_KEY AS NEW_WK_KEY,
			CUR.WK_KEY AS CUR_WK_KEY,
			$MAX_ACITVE_SIZE_SCALE + ROW_NUMBER() OVER ( ORDER BY (SELECT NULL)) AS NEW_ACTIVE_VALUE,
			TEMP_ID,
			NEW.SCALE_ID,
			CUR.active_size_scale,
			NEW.SCALE_NAME,
			NEW.CREATED_YEAR,
			NEW.CREATED_SEASON,
			NEW.DEPARTMENT,
			CASE WHEN NEW.WK_KEY > CUR.WK_KEY THEN 1
				WHEN NEW.WK_KEY = CUR.WK_KEY THEN 2
				WHEN NEW.WK_KEY < CUR.WK_KEY THEN 3
				ELSE -1 END AS DATA_FLAG
	FROM(SELECT DISTINCT WK_KEY,
						TEMP_ID,
						SCALE_ID,
						CREATED_YEAR,
						CREATED_SEASON,
						SCALE_NAME,
						DEPARTMENT,
						CREATED_DTTM
						FROM HT_SANDBOX_PRD_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV AS TB1
							JOIN HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.STG_D_SIZE_SCALE_HEAD AS TB2
								ON CAST(TB2.CREATED_DTTM AS DATE) = TB1.DAY_KEY
						WHERE NEW_SIZE_SCALE = '0' 
						AND TEMP_ID NOT IN (SELECT TEMP_ID FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_NEW_YEAR_TEMP))  NEW
	INNER JOIN (SELECT DISTINCT WK_KEY,
							SIZE_SCALE_ID,
							ACTIVE_SIZE_SCALE,
							SEASON_YEAR,
							SEASON,
							SCALE_NAME,
							DEPARTMENT,
							UPDATED_DATE
							FROM HT_SANDBOX_PRD_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV AS TB1
							JOIN  HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_HEADER AS TB2
								ON CAST(TB2.UPDATED_DATE AS DATE) = TB1.DAY_KEY) CUR
			ON CUR.SIZE_SCALE_ID = NEW.SCALE_ID
			AND CUR.SEASON_YEAR  = NEW.CREATED_YEAR					
			AND CUR.SEASON = NEW.CREATED_SEASON					
			AND CUR.DEPARTMENT = NEW.DEPARTMENT;;
```
## Updating Model Detail Table <a name="Updating_DT"></a>

```sql
INSERT INTO HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_DETAIL (SCALE_ID,
																SIZE_PROFILE_DETAIL_ID,
																ACTIVE,
																PROFILE_NAME,
																SIZE_INDEX,
																SIZE_ID,
																SIZE_PERCENT,
																MIN_UNITS,
																SEASON,
																SEASON_YEAR)
		SELECT TB1.SCALE_ID,
			MAX_ACTIVE_SS,
			0 AS ACTIVE,
			TB1.PROFILE_NAME,
			TB1.SIZE_INDEX,
			TB1.SIZE_ID,
			TB1.SIZE_PERCENT,
			TB1.MIN_UNITS,
			TB2.CREATED_YEAR,
			TB2.CREATED_SEASON
	FROM HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.SIZE_SCALE_DETAIL TB1
	INNER JOIN (SELECT * 
					FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_TEMP
					where DATA_FLAG = '1'
					order by SCALE_ID) AS TB2
		ON TB1.TEMP_ID = TB2.TEMP_ID;
```
## Updating Size Scale Profile Table <a name="updating_PT"></a>

```sql
INSERT INTO HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_PROF (SIZE_SCALE_ID, 
														SIZE_PROFILE_DETAIL_ID,
														SCALE_NAME,
														STORE_NUMBER,
														PROFILE_NAME)
	SELECT TB1.SCALE_ID,
			TB2.MAX_ACTIVE_SS,
			TB1.SCALE_NAME,
			TB1.STORE_NUMBER,
			TB1.PROFILE_NAME
	FROM HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.SIZE_SCALE_PROF TB1
	INNER JOIN (SELECT *
					FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_TEMP
					where DATA_FLAG = '1'
					order by SCALE_ID) TB2
		ON TB1.TEMP_ID = TB2.TEMP_ID;
```
## Updating Size Scale Header Table <a name="updating_HT"></a>

```sql
UPDATE HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_HEADER
		SET ACTIVE_SIZE_SCALE = TB2.NEW_ACTIVE_VALUE
		FROM (SELECT * 
			FROM HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_HEADER AS TB1
			INNER JOIN (SELECT * 
					FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_TEMP
					where DATA_FLAG = '1'
					order by SCALE_ID) AS TB2
			ON TB2.SCALE_ID = TB1.SIZE_SCALE_ID
			AND	TB2.SCALE_NAME = TB1.SCALE_NAME
			AND	TB2.CREATED_YEAR = TB1.SEASON_YEAR
			AND	TB2.CREATED_SEASON = TB1.SEASON
			AND	TB2.DEPARTMENT = TB1.DEPARTMENT) as TB2;
```
## Removing Detail size Scale Records <a name="removeing_SSRD"></a>

```sql
DELETE FROM HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_DETAIL 
	WHERE SCALE_ID IN ( SELECT DISTINCT SCALE_ID 
							FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_TEMP 
							WHERE DATA_FLAG = '2' )
	AND SIZE_PROFILE_DETAIL_ID IN (SELECT DISTINCT ACTIVE_SIZE_SCALE 
									FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_TEMP 
									WHERE DATA_FLAG = '2');
```
## Inserting Size Scales Detail Data <a name="inserting_SSDD"></a>


```sql
INSERT INTO HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_DETAIL (SCALE_ID,
																SIZE_PROFILE_DETAIL_ID,
																ACTIVE,
																PROFILE_NAME,
																SIZE_INDEX,
																SIZE_ID,
																SIZE_PERCENT,
																MIN_UNITS,
																SEASON,
																SEASON_YEAR)
	SELECT TB1.SCALE_ID,
			TB2.MAX_ACTIVE_SS,
			0 AS ACTIVE,
			TB1.PROFILE_NAME,
			TB1.SIZE_INDEX,
			TB1.SIZE_ID,
			TB1.SIZE_PERCENT,
			TB1.MIN_UNITS,
			TB2.CREATED_YEAR,
			TB2.CREATED_SEASON
	FROM HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.SIZE_SCALE_DETAIL TB1
	INNER JOIN (SELECT * FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_TEMP WHERE DATA_FLAG = '2') AS TB2
		ON TB1.TEMP_ID = TB2.TEMP_ID;
```
## Removed Size Scales from Profile Table <a name="removing_SSDP"></a>

```sql
DELETE FROM HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_PROF 
	WHERE SIZE_SCALE_ID IN (SELECT DISTINCT SCALE_ID FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_TEMP WHERE DATA_FLAG = '2')
	AND SIZE_PROFILE_DETAIL_ID IN (SELECT DISTINCT ACTIVE_SIZE_SCALE FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_TEMP WHERE DATA_FLAG = '2');
```
## Inserting Size Scale into Profile Table <a name="inserting_SSPD"></a>

```sql
INSERT INTO HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_PROF (SIZE_SCALE_ID, 
														SIZE_PROFILE_DETAIL_ID,
														SCALE_NAME,
														STORE_NUMBER,
														PROFILE_NAME)
	SELECT TB1.SCALE_ID,
			TB2.ACTIVE_SIZE_SCALE,
			TB1.SCALE_NAME,
			TB1.STORE_NUMBER,
			TB1.PROFILE_NAME
	FROM HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.SIZE_SCALE_PROF TB1
	INNER JOIN (SELECT * FROM HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_TEMP WHERE DATA_FLAG = '2') TB2
		ON TB1.TEMP_ID = TB2.TEMP_ID;
```
## Setting Active to Zero <a name='setting_active'></a>

```sql
UPDATE HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_DETAIL
		SET ACTIVE = 0;

UPDATE HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_DETAIL AS T1
		SET ACTIVE = 1
		FROM HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_HEADER AS T2
		WHERE T1.SIZE_PROFILE_DETAIL_ID = T2.ACTIVE_SIZE_SCALE;
```
## Dropping Temp Table <a name='dropping_table'></a>
```sql
DROP TABLE HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_TEMP
DROP TABLE HT_SANDBOX_PRD_DB.ALLOC_TMP.SIZE_SCALE_NEW_YEAR_TEMP;
```