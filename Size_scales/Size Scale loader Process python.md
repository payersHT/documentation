# Size Scale loader Process python

This is the full process for updating and loading size scales.

## importing package and setting global variables

```python
# loading packages
import pandas as pd
import numpy as np
import pyodbc
import sqlalchemy
import urllib
import os
import re
import shutil
import datetime as datetime
import snowflake.connector
from pandas import DataFrame

DB_WAREHOUSE = "ALLOC_RPT_PRD_WH"
DATABASE = "HT_SANDBOX_PRD_DB"
DB_LOADING_DATA = "HT_SANDBOX_PRD_DB.ALLOC_STG;"
HEAD_TABLE = "HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_HEADER"
DETAIL_TABLE = "HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_DETAIL"
PROFILE_TABLE = "HT_SANDBOX_PRD_DB.ALLOC_MODEL.SIZE_SCALE_PROF"
```

## Function Loading Size scale Data

```python
# add user name and password
def loading_size_data(load_DF, type_load, SF_UN, SF_PW):
    #Will load diata for size scales to different types of database tables

    #Parameters:
    #    load_DF (dataframe): A dataframe that will be loaded into a database. Must be given
    #    type_load (str): A string that will be used to know what table to load the size scale 
    #    SF_UN (str): The user name for snowflake. Must be given type
    #    SF_PW (str): The Password for Snowflake

    #Returns:
    #    reverse(str1):The string which gets reversed. 
	if type_load == "" or type_load not in [
		"size_scale_detail",
		"size_scale_prof",
		"size_scale_header",
	]:
		raise ValueError("Please select that data that is being loaded")
		return -1

	ctx = snowflake.connector.connect(
		user=SF_UN,
		password=SF_PW,
		# this can be found in the URL of snowflake website
		account="hottopic.east-us-2.azure",
		database=DATABASE,
		schema="ALLOC_STG_MODEL",
		# This needs to be set so you can write to the staging and normal tables
		role="HT_DBA_PRD",
	)
	cs = ctx.cursor()

	if type_load == "size_scale_detail":
		try:
			# write csv file
			load_DF.to_csv("temp_detail_load.csv", index=False, header=False)
			# Changing to the currect WAREHOUSE
			cs.execute(f" USE WAREHOUSE {DB_WAREHOUSE}")
			print(DataFrame(cs.fetchall()))
			cs.execute("TRUNCATE HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.SIZE_SCALE_DETAIL;")
			# have to state the location of the file path, the "@%" is letting snowflake know to
			# put it into the staging table for tables it will go to.
			cs.execute(
				"PUT file://temp_detail_load.csv @%SIZE_SCALE_DETAIL OVERWRITE = TRUE"
			)
			# Copy the data into a new table form the stage table.
			# cs.execute("TRUNCATE SIZE_SCALE_HEADER_TEMP;")
			cs.execute("COPY INTO SIZE_SCALE_DETAIL FROM @%SIZE_SCALE_DETAIL;")
			os.remove("temp_detail_load.csv")
		finally:
			cs.close()
		ctx.close()
	elif type_load == "size_scale_prof":
		try:
			# write the csv file
			load_DF.to_csv("temp_prof_load.csv", index=False, header=False)
			# Changing to the currect WAREHOUSE
			cs.execute(f" USE WAREHOUSE {DB_WAREHOUSE}")
			print(DataFrame(cs.fetchall()))
			cs.execute("TRUNCATE HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.SIZE_SCALE_PROF;")
			# have to state the location of the file path, the "@%" is letting snowflake know to
			# put it into the staging table for tables it will go to.
			cs.execute(
				"PUT file://temp_prof_load.csv @%SIZE_SCALE_PROF OVERWRITE = TRUE"
			)
			# Copy the data into a new table form the stage table.
			# cs.execute("TRUNCATE SIZE_SCALE_HEADER_TEMP;")
			cs.execute("COPY INTO SIZE_SCALE_PROF FROM @%SIZE_SCALE_PROF ;")
			os.remove("temp_prof_load.csv")
		finally:
			cs.close()
		ctx.close()
	elif type_load == "size_scale_header":
		try:
			# write the csv file
			load_DF[["temp_id","scale_id","size_profile_detail_id",
					"created_year","created_season","scale_name","scale_rename","department",
					"class","size_count","size_range","profile_count","gender","Creator","body_style","genre","license","Notes",
					"row_num","created_dttm","New_size_scale"]].to_csv('temp_header_load.txt',index = False,header=False,na_rep = '',sep="|")
			# Changing to the currect WAREHOUSE
			cs.execute(f"USE WAREHOUSE {DB_WAREHOUSE};")
			print(DataFrame(cs.fetchall()))
			cs.execute("TRUNCATE HT_SANDBOX_PRD_DB.ALLOC_STG_MODEL.STG_D_SIZE_SCALE_HEAD;")
			# have to state the location of the file path, the "@%" is letting snowflake know to
			# put it into the staging table for tables it will go to.
			cs.execute(
				"PUT file://temp_header_load.txt @%STG_D_SIZE_SCALE_HEAD OVERWRITE = TRUE"
			)
			# Copy the data into a new table form the stage table.
			# cs.execute("TRUNCATE SIZE_SCALE_HEADER_TEMP;")
			cs.execute("COPY INTO STG_D_SIZE_SCALE_HEAD FROM @%STG_D_SIZE_SCALE_HEAD file_format = (format_name = csvformat);")
			os.remove("temp_header_load.txt")
		finally:
			cs.close()
		ctx.close()
```

## Preparing and Loading Size Scale

```python
def Preparing_and_loading_size_scales(SF_UN, SF_PW):
# need to load in all of the files in the directory
	# read in using the map functions
	os.chdir("//ntserver/pa/PA_DB/ETL")
	file_list = os.listdir()
	if len(file_list) == 0:
		print("No files in ETL folder")
		return
	file_list = pd.DataFrame(file_list)
	file_list["file_type"] = ""
	for file in range(len(file_list)):
		file_name = file_list.loc[file, 0]
		file_name_value = file_name.split("_")[0]
		file_list.loc[file, "file_type"] = file_name_value
	print(file_list)

	incoming_size_header = pd.DataFrame()
	filter_file_list = file_list[file_list.file_type == "sizehdr"][0]

	for file in filter_file_list:
		temp_file = pd.read_csv(file, sep="|")
		incoming_size_header = incoming_size_header.append(temp_file)

	incoming_size_header.created_year = incoming_size_header.created_year.astype(str)
	incoming_size_header["New_size_scale"] = 0
	incoming_size_header["Cat_value"] = (incoming_size_header.scale_name+ "_"+ (incoming_size_header.created_year)+ "_"+ incoming_size_header.created_season)

	filter_file_list = file_list[file_list.file_type == "sizescales"][0]
	incoming_size_sizescales = pd.DataFrame()

	for file in filter_file_list:
		temp_file = pd.read_csv(file, sep="|")
		incoming_size_sizescales = incoming_size_sizescales.append(temp_file)

	filter_file_list = file_list[file_list.file_type == "sizeprof"][0]
	incoming_size_sizeprof = pd.DataFrame()

	for file in filter_file_list:
		temp_file = pd.read_csv(file, sep="|")
		incoming_size_sizeprof = incoming_size_sizeprof.append(temp_file)

	ctx = snowflake.connector.connect(
		user=SF_UN,
		password=SF_PW,
		# this can be found in the URL of snowflake website
		account="hottopic.east-us-2.azure",
		database=DATABASE,
		schema="ALLOC_MODEL",
		# This needs to be set so you can write to the staging and normal tables
		role="HT_DBA_PRD",
	)
	cs = ctx.cursor()
	try:
		# Changing to the currect warehouse
		cs.execute(f" USE WAREHOUSE {DB_WAREHOUSE}")
		print(DataFrame(cs.fetchall()))
		max_data = cs.execute(
			f"""SELECT (SELECT MAX(size_scale_id) as Max_size_scale_id FROM {HEAD_TABLE}) as Max_size_scale_id,
								(SELECT max(size_profile_detail_id) FROM {DETAIL_TABLE}) as size_profile_detail_id_DID,
								(SELECT MAX(size_profile_detail_id) FROM {PROFILE_TABLE}) as size_profile_detail_id_SP"""
		)
		max_data_DF = pd.DataFrame(
			[tuple(t) for t in max_data],
			columns=(
				"max_size_scale_id",
				"size_profile_detail_id_DID",
				"size_profile_detail_id_SP",
			),
		)
		# Making the counting varables
		max_size_ID = pd.array(max_data_DF.max_size_scale_id)
		max_size_profile_DID = pd.array(max_data_DF.size_profile_detail_id_DID)
		size_scale_ID = cs.execute(
			f"""SELECT DISTINCT SIZE_SCALE_ID,SCALE_NAME FROM {HEAD_TABLE}"""
		)
		size_scale_ID_DF = pd.DataFrame(
			[tuple(t) for t in size_scale_ID], columns=("size_scale_id", "scale_name")
		)
		header_table = cs.execute(
			f"""SELECT SIZE_SCALE_ID,ACTIVE_SIZE_SCALE,
									SEASON_YEAR,SEASON, SCALE_NAME,DEPARTMENT,CLASS,
									SIZE_COUNT,SIZE_RANGE,SIZE_LIST,PROFILE_COUNT,
									GENDER,CREATOR,BODY_STYLE,GENRE,LICENSE,NOTES,
									CONCAT(SCALE_NAME,'_',SEASON_YEAR,'_',SEASON) AS CAT_VALUE,UPDATED_DATE
									FROM {HEAD_TABLE}"""
		)
		header_table_DF = pd.DataFrame(
			[tuple(t) for t in header_table],
			columns=("size_scale_id","active_size_scale",
					"season_year","season","scale_name",
				"department","class","size_count",
				"size_range","size_list","profile_count",
				"gender","creator","body_style","genre",
				"license","notes","cat_value","updated_date",),)

	finally:
		cs.close()
	ctx.close()

	# For Size Scales that already have a size_scales_ID
	#getting the newest Value for size scale
	AE_header = pd.DataFrame(incoming_size_header[pd.notnull(incoming_size_header["scale_id"])].groupby(["scale_name","Cat_value"],as_index = False)["created_dttm"].max())

	# For size scales that are new and dont have a scale ID.
	# Adding size scales, updating size_profile detail ID.
	new_header = pd.DataFrame(incoming_size_header[pd.isnull(incoming_size_header["scale_id"])].groupby(["scale_name","Cat_value"],as_index = False)["created_dttm"].max())

	new_sch = incoming_size_header[(incoming_size_header["Cat_value"].isin(list(new_header["Cat_value"]))) & 
                    (incoming_size_header["created_dttm"].isin(list(new_header["created_dttm"])))][["scale_name","temp_id","created_dttm"]]

	size_scale_filter = pd.concat([AE_header,new_header])

	#Filter size scale headers
	incoming_size_header = incoming_size_header[(incoming_size_header["Cat_value"].isin(list(size_scale_filter["Cat_value"]))) & 
                                            (incoming_size_header["created_dttm"].isin(list(size_scale_filter["created_dttm"])))]

	new_sch["scale_id_value"] = 0
	new_sch = new_sch[['scale_name','temp_id','created_dttm','scale_id_value']]

	for size_name in range(len(new_sch)):
		new_sch.iloc[size_name, 3] = max_size_ID + 1
		max_size_ID = max_size_ID + 1

	for size_name in range(len(incoming_size_header)):
		if pd.isnull(incoming_size_header.iloc[size_name, 1]):
			print(size_name)
			scale_id = pd.array(new_sch[new_sch.temp_id == incoming_size_header.iloc[size_name, 0]].scale_id_value)
			temp_id_value = incoming_size_header.iloc[size_name, 0]
			size_profile_DID = max_size_profile_DID + 1
			incoming_size_header.iloc[size_name, 1] = scale_id
			incoming_size_header.iloc[size_name, 21] = 1
			incoming_size_header.iloc[size_name, 2] = size_profile_DID
			# updating the size scale prof and size_sizeScales

			incoming_size_sizeprof.loc[incoming_size_sizeprof["temp_id"] == temp_id_value, "scale_id"] = scale_id[0]
			incoming_size_sizeprof.loc[incoming_size_sizeprof["temp_id"] == temp_id_value,"size_profile_detail_id",] = size_profile_DID[0]
			incoming_size_sizescales.loc[incoming_size_sizescales["temp_id"] == temp_id_value, "scale_id"] = scale_id[0]
			incoming_size_sizescales.loc[incoming_size_sizescales["temp_id"] == temp_id_value,"size_profile_detail_id",] = size_profile_DID[0]

			# updating the max_size_profile_DID value
			max_size_profile_DID = max_size_profile_DID + 1

	loading_header_data = incoming_size_header[incoming_size_header.columns[:-1]]
	loading_header_data["size_list"] = loading_header_data["size_list"].str.replace(",", "-")
	loading_header_data["created_dttm"] = pd.to_datetime(loading_header_data["created_dttm"])

	loading_size_profile = incoming_size_sizeprof[incoming_size_sizeprof["temp_id"].isin(incoming_size_header["temp_id"])]
	loading_size_profile["created_dttm"] = pd.to_datetime(loading_size_profile["created_dttm"])

	loading_size_scales = incoming_size_sizescales[incoming_size_sizescales["temp_id"].isin(incoming_size_header["temp_id"])]
	loading_size_scales["created_dttm"] = pd.to_datetime(loading_size_scales["created_dttm"])

	loading_size_data(loading_header_data, "size_scale_header", SF_UN, SF_PW)
	loading_size_data(loading_size_profile, "size_scale_prof", SF_UN, SF_PW)
	loading_size_data(loading_size_scales, "size_scale_detail", SF_UN, SF_PW)

	updating_size_scale_database(SF_UN, SF_PW)
```
## Function updating Size Scale Database

```python
def updating_size_scale_database(SF_UN, SF_PW):
	ctx = snowflake.connector.connect(
		user=SF_UN,
		password=SF_PW,
		# this can be found in the URL of snowflake website
		account="hottopic.east-us-2.azure",
		database=DATABASE,
		schema="ALLOC_STG_MODEL",
		# This needs to be set so you can write to the staging and normal tables
		role="HT_DBA_PRD",
	)
	cs = ctx.cursor()

	try:
		print('setting warehouse')
		cs.execute(f" USE WAREHOUSE {DB_WAREHOUSE}")
		print(DataFrame(cs.fetchall()))

		print("updating size scale")
		cs.execute(update_size_scale_sql_1)
		print(DataFrame(cs.fetchall()))

		print('insert new size scale')
		cs.execute(INSERT_NEW_SIZE_SCALE_DETAIL_2)
		print(DataFrame(cs.fetchall()))

		print('insert new size scale')
		cs.execute(INSERT_NEW_SIZE_SCALE_PROF_3)
		print(DataFrame(cs.fetchall()))

		print('Setting first max value')
		cs.execute(SETTING_MAX_ACTIVE_4)
		print(DataFrame(cs.fetchall()))

		print('Creating new temp table')
		cs.execute(CREATING_NEW_SEASON_TEMP_5)
		print(DataFrame(cs.fetchall()))

		print('Inserting into temp table')
		cs.execute(INSERT_NEW_SEASON_TEMP_6)
		print(DataFrame(cs.fetchall()))

		print('Insert new season into header')
		cs.execute(INSERT_NEW_SEASON_HEADER_7)
		print(DataFrame(cs.fetchall()))

		print('Insert new season into profile')
		cs.execute(INSERT_NEW_SEASON_PROFILE_8)
		print(DataFrame(cs.fetchall()))

		print('Insert new season into detail')
		cs.execute(INSERT_NEW_SEASON_DETAIL_9)
		print(DataFrame(cs.fetchall()))

		print('setting max active')
		cs.execute(SETTING_MAX_ACTIVE_10)
		print(DataFrame(cs.fetchall()))

		print('making new temp table')
		cs.execute(CREATING_TEMP_TABLE_11)
		print(DataFrame(cs.fetchall()))

		print('insert into temp table')
		cs.execute(INSERT_INTO_TEMP_TABLE_12)
		print(DataFrame(cs.fetchall()))

		print('updateing detail')
		cs.execute(UPDATEING_MODEL_DETAIL_13)
		print(DataFrame(cs.fetchall()))

		print('updateing prof table')
		cs.execute(UPDATEING_MODEL_PROF_14)
		print(DataFrame(cs.fetchall()))

		print('updating header table')
		cs.execute(UPDATING_MODEL_HEADER_15)
		print(DataFrame(cs.fetchall()))

		print('remove detail records')
		cs.execute(REMOVE_DETAIL_RECORDS_16)
		print(DataFrame(cs.fetchall()))

		print('insert detail same week')
		cs.execute(INSERT_DETAIL_SAME_WEEK_17)
		print(DataFrame(cs.fetchall()))

		print('remove profile same week')
		cs.execute(REMOVE_PROFILE_SAME_WEEK_18)
		print(DataFrame(cs.fetchall()))

		print('insert profile same week')
		cs.execute(INSERT_PROFILE_SAME_WEEK_19)
		print(DataFrame(cs.fetchall()))

		print('set active to zero')
		cs.execute(ACTIVE_SET_0_20)
		print(DataFrame(cs.fetchall()))

		print('set active to 1')
		cs.execute(ACTIVE_SET_1_21)
		print(DataFrame(cs.fetchall()))

		print('drop table 1')
		cs.execute(DROP_TABLE_22)
		print(DataFrame(cs.fetchall()))

		print('drop table 2')
		cs.execute(DROP_TABLE_2_23)
		print(DataFrame(cs.fetchall()))

	finally:
		cs.close()
	ctx.close()

```