# Geo-Lasso Process # 

We will be going over the geo-lasso code and what each section is doing, and how the data needs to be preped before it can be loaded into Snowflake and then into looker.

## Geo-Lasso Full Process ## 

This is the full process for calculating the distance from a stores zipcode to every zipcode in the US. This process will take about 1 hour to run for every 100 stores.


```r
us_zip_closest_to_store <- function(){
   
   # This script will create a matrix of distances from each US store to all US zip codes and
   # find the store that has the shortest distance to the US zip codes
   # Developed by: Martin Quach
   # Date Revised: 06-22-17
   
   #Start timer
   ptm <- proc.time()
   
   #Load Geosphere library
   library(geosphere)
   
   #Set working directory
   setwd("P:/PA STAFF/Martin Q/Analysis/R Analysis/Zip Code Grouping/US Zip Codes")
```
The directory above contains all of the information needed to run the full process, there is also a backup in my person folder as well. The backup

```r   
   #Read in US zip codes
   US_Postal_Codes <- read.csv("P:/PA STAFF/Martin Q/Analysis/R Analysis/Zip Code Grouping/US Zip Codes/uszipcodes.csv")
   
   #Read in store numbers
   US_Store_Latitude_Longitude <- read.csv("P:/PA STAFF/Martin Q/Analysis/R Analysis/Zip Code Grouping/US Zip Codes/StoresZipCodeDataUS.csv")
```

This section is used to read in the information needed to calculate the distance for each zipcode. The format for US_postal_codes is a column for the zipcode , lat and longe data. It is the same format for US_stores_latitude_Longitude but insteade of zipcode it has store number.

```r

   #Create dataframe of Stores and Postal Codes
   dfpostal <- data.frame(cbind(longitude = US_Postal_Codes$longitude, latitude = US_Postal_Codes$latitude))
   dfstore <- data.frame(cbind(storenum = US_Store_Latitude_Longitude$StoreNum, longitude = US_Store_Latitude_Longitude$Long, latitude = US_Store_Latitude_Longitude$Lat))
   dfpostalname <- data.frame(cbind(postalcode = US_Postal_Codes$zip))
   
   #Find length of postal and store
   postalcodevector <- as.vector(dfpostalname$postalcode)
   lenpostal <- length(postalcodevector)
   storenumvector <- as.vector(dfstore$storenum)
   lenstorenum <- length(storenumvector)

   #Create empty dataframe for distances and give them names
   #Store number(x-axis), US Zips(y-axis)
   dfdistance <- data.frame(matrix(ncol = lenstorenum, nrow = lenpostal))
   colnames(dfdistance) <- storenumvector
   rownames(dfdistance) <- postalcodevector
```
Prepping the data for the calculation.

```r   
   #Create array of each lat and long
   #For postal:
   latpostal <- dfpostal$latitude
   longpostal <- dfpostal$longitude
   #For stores:
   latstores <- dfstore$latitude
   longstores <- dfstore$longitude
``` 
This is what the function will use to iterate over.
```r
   #Extrapolate latitude and longitude of those stores
   for(i in 1:lenpostal){
     lat = as.double(latpostal[i])
     long = as.double(longpostal[i])
     for(j in 1:lenstorenum){
       lat1 = latstores[j]
       long1 = longstores[j]
       
       #Calculate distance and put into dataframe
       #Divide by 1609 to convert to miles
       dfdistance[i,j] <- distm(c(long, lat), c(long1, lat1), fun = distHaversine)[,1]/1609
     }
   }
```
This section is what calculates the distance from each zipcode to each store. NOTE THE FUNCTION distHarversine is used. This is not the basic distance formula we were taught in school $\left(\sqrt{(x_2 - x_1)^2 + (y_2 - y_1)^2}\right)$ but it uses the Haversine formula. 



$distance =2rarcsin \left( \sqrt{sin^2(\frac{\phi_1 -\phi_2}{2})+ cos\phi_1 *cos\phi_2*sin^2(\frac{\lambda_1 -\lambda_2}{2})}\right)$


* $\lambda_1$ and $\lambda_2$ are the latitude of point 1 and latitude of point 2 
* $\phi_1$ and $\phi_2$ are the longitude of point 1 and longitude of point 2.
* r is the radius of the sphere.

Once all of the distances are calculated for every combination of zipcode and store, we then devide it by 1609 to convert the distance from Km to miles.

```r
   #Go through each row of the matrix to find zip code with least distance to store
   result <- t(sapply(seq(nrow(dfdistance)), function(i){
     j <- which.min(dfdistance[i,])
     c(paste(rownames(dfdistance)[i],colnames(dfdistance)[j], sep = "/"), dfdistance[i,j])
   }))
```
The Final step will find the cloest store to each zipcode.
```r   
   #Create csv files
   write.csv(dfdistance,file = "dfdistance.csv")
   write.csv(result, file = "zip_closest_to_store.csv")
   
   #Stop timer
   proc.time() - ptm
}
```

This section output a dfdistance which is a matrix of every combination of zipcode and store.


## Geo-Lasso for Canada Stores ##

This process is almost the same as for US stores with one majore difference in how the data is loaded. 

```r
  Canada_Postal_Codes <- read.csv("P:/PA STAFF/Martin Q/Analysis/R Analysis/Zip Code Grouping/Canada Zip Codes/Canada Postal Codes.csv",
                                    colClasses = c(zip ="character"))

```
An extra argument colClasses is used, this is needed because some Canada stores have letters in them and the data needs to be read in as a character or else it will throw an error.



## Removing Stores from Geo-Lasso ##

When we need to remove a store from Geo-lasso instead of running the full process again which can take upto 5 hours to run. We can just load the dfdistance file and remove the store column. We can do this because the store distances are independent of each other, and all we will need to do is rerun the function that finds the closest store.

```r
library(tidyverse)
library(geosphere)
setwd("P:/PA STAFF/Martin Q/Analysis/R Analysis/Zip Code Grouping/US Zip Codes")

#reading in the last newest df distance file
DF_distance <- read_csv("P:/PA STAFF/Martin Q/Analysis/R Analysis/Zip Code Grouping/US Zip Codes/dfdistance_raw_202010216.csv",col_names = TRUE)
```
Reading in the full dfdistance file.
```r
#adding the list of store(s) that need to be removed.
new_DF_distance <- DF_distance %>% select(one_of('1','2','3','4','5','6','7','8',
                                                 '9','11','12','16','17','18','19','20','21','22','23','24','25','26',
                                                 '27','28','29','30','31','32','33','35','36','37','38','39','41','42',
                                                 '43','44','45','46','48','49','52','53','54','55','56','57','58','59',
                                                 '60','61','62','63','64','65','66','67','68','70','71','73','74','75',
                                                 '76','78','79','80','82','83','84','85','87','88','89','90','92','93',
                                                 '94','95','96','97','98','99','100','101','103','105','106','108','109',
                                                 '110','111','112','113','114','115','116','117','119','120','121','122',
                                                 '123','124','125','127','128','129','130','131','132','134','135','136',
                                                 '137','138','139','140','142','143','146','147','148','149','150','151',
                                                 '152','153','154','157','158','159','160','161','162','164','165','166','168',
                                                 '170','171','172','173','174','175','176','177','178','179','180','183',
                                                 '184','185','187','188','189','190','191','192','193','194','195','196',
                                                 '197','198','200','201','202','204','205','206','208','210','211','212',
                                                 '213','214','215','217','218','219','220','221','223','224','225','226',
                                                 '227','228','229','230','232','233','234','236','237','239','240','241',
                                                 '243','244','246','247','248','249','250','251','254','255','256','257',
                                                 '259','261','262','263','264','265','266','267','268','269','270','271',
                                                 '272','273','274','275','276','277','279','281','282','283','284','285',
                                                 '286','288','289','290','293','294','295','296','298','302','303','304',
                                                 '305','306','308','311','312','314','316','317','318','319','320','321',
                                                 '322','323','324','325','326','328','331','332','333','334','336','337','338',
                                                 '339','340','341','343','344','345','346','348','349','350','351','352',
                                                 '354','355','356','357','358','362','363','364','365','366','367','368',
                                                 '370','371','372','373','374','375','376','378','379','380','381','382',
                                                 '386','387','388','389','390','391','392','395','397','399','400','401',
                                                 '402','403','404','405','407','409','412','414','415','416','417','419',
                                                 '421','422','423','424','425','426','427','428','429','431','432','433',
                                                 '434','435','436','437','438','439','440','441','442','445','446','447',
                                                 '448','449','450','451','452','453','454','456','457','459','460','461',
                                                 '462','463','464','465','466','467','468','469','470','473','474','475',
                                                 '476','478','479','481','483','488','491','492','493','494','495','496',
                                                 '497','498','499','500','501','503','504','505','506','507','510','511',
                                                 '512','513','514','516','517','518','519','521','522','523','524','525',
                                                 '526','528','530','531','534','536','537','538','541','543','545','546',
                                                 '547','549','550','551','552','554','555','556','557','558','563','564',
                                                 '565','566','568','569','570','572','573','574','575','576','577','578',
                                                 '579','580','581','582','585','586','587','588','589','590','592','593','594',
                                                 '595','597','598','599','600','601','602','603','604','606','608','609',
                                                 '611','612','613','614','617','620','622','623','624','626','628','629',
                                                 '630','631','632','633','634','639','642','643','645','647','648','654',
                                                 '656','657','659','660','663','664','665','666','668','671','672','673',
                                                 '678','687','688','689','695','696','698','702','705','706','712','715',
                                                 '716','717','719','725','726','728','730','731','732','733','734','736',
                                                 '738','739','740','741','742','743','744','745','746','747','748','749',
                                                 '750','752','753','755','757','758','759','760','761','762','765','766',
                                                 '767','768','769','770','771','772','773','775','776','777','778','779',
                                                 '781','782','783','784','785','787','788','790','791','793','794','795',
                                                 '796','797','799','800','803','804','805','806','807','808','809','810',
                                                 '811','812','814','816','817','819','820','821','822','823','824','825'))
```
This is the list of the store that will be left over.

```r
#checking the stores were removed
length(names(DF_distance))
length(names(new_DF_distance))

#prepding the data for the sorting algorithum
new_DF_distance <- as.data.frame(new_DF_distance)
rownames(new_DF_distance) <-  (new_DF_distance$X1)
dfdistance <- as.data.frame( new_DF_distance)
dfdistance$X1 <- NULL
dfdistance_new <- dfdistance
```
Preping the data to find the minimum distance. Note that we make the row names equal to the first column that is read into the (X1) and we then set that column to NULL (delete it). If this step is not done it will cause an error in the output of the file.

```r
#Go through each row of the matrix to find zip code with least distance to store
result <- t(sapply(seq(nrow(dfdistance_new)), function(i){
  j <- which.min(dfdistance_new[i,])
  c(paste(rownames(dfdistance_new)[i],colnames(dfdistance_new)[j], sep = "/"), dfdistance_new[i,j])
}))

#getting the results data into a more readable format
result_1 <- as.data.frame(result)
result_2 <- separate(result_1,V1,c("Zipcode","Store_number"))
names(result_2) <- c("Zipcode","Store_number","distance")

#outputing the files
setwd("P:/PA STAFF/Martin Q/Analysis/R Analysis/Zip Code Grouping/US Zip Codes")
write.csv(dfdistance, "dfdistance_raw_20220121.csv")
write.csv(result_2, file = "zip_closest_to_store_20220121.csv")
```
This is all the same as running the full model.


## Adding A Store ##

Only BoxLunch as had to add stores in the last 2 years and with the number of stores being so small I would just run the full process. There store count is getting close to 200, and the process will now start to take upwords of two hours to run. You can make this process a lot fast if you only calculate the distance for the stores being added to each zipcode then JOIN the two dataframes together. This would be a great project for both of you to work on and will leave it as an exercise.


## Loading Data into Looker ##

At the time of writing this the maximum distance a store can be from a zipcode is 25 mile for HotTopic and 30 miles for Boxlunch. This number may change so be sure to ask store planning.

This is the format the CSV files needs to be in when it is loaded.

| Division | Zip Code | Closet Store | Distance(mi) | Country | GeoFence Store |
| -------- | -------- | -------- | -------- | -------- | -------- |
| 1 | 210 | 557 | 12.2757 | US | 557 |
| 1 | 501 | 52 | 26.2757 | US | Out Of Range |

When the file is created you will need to put in the following location ```\\nashare4\pa\General Use\EDW Stage\Stores```. This drive requies permission to access it and so a manager will need to place it. Make sure the CSV has the name "HT Geo Lasso Zip Store Table".